#!flask/bin/python
from flask import Flask, jsonify
import json
import configparser
import logging
import random
import requests

#############################################
#                                           #
#   Get Existing application config data    #
#                                           #
#############################################

config = configparser.ConfigParser()
config.read("dictionaryAccess.conf")

base_url = config['DEFAULT']['base_url']
app_id = config['DEFAULT']['app_id']
key = config['DEFAULT']['app_key']
lang = config['DEFAULT']['language']

#############################################
#                                           #
#   Setting up logging information          #
#                                           #
#############################################

#logging.basicConfig(filename=config['LOGGING']['filename'], filemode='w', level=logging.DEBUG, format='%(levelname)s %(asctime)s %(message)s',  datefmt='%m/%d/%Y %I:%M:%S %p')

app = Flask(__name__)

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol', 
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web', 
        'done': False
    }
]

def build_and_execute_request(path, url_filters, url_params):
    url = base_url + path + lang 
    if bool(url_filters):
        url = url + "/"
        for filter_key in url_filters.keys():
            url = url + filter_key + '=' + url_filters[filter_key] + ';'

    if bool(url_params):
        url = url + '?'
        for param_key in url_params.keys():
            url = url + param_key + '=' + url_params[param_key] + '&'

        url = url[:-1]

    print (url)
    r = requests.get(url, headers = {'app_id': app_id, 'app_key': key})
   
    return r.json()

@app.route('/todo/api/v1.0/tasks', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})

@app.route('/dictionary/v1/domainListRandom',methods=['GET'])
def fetchRandomDomains():
    all_domains = str(config['DATA_PARAMS']['domains']).split(',')
    num_domains = len(all_domains) - 1
    
    domain_indices = random.sample(range(0, num_domains),5)

    path = "/wordlist/"

    domain_words_list = []
    list_of_domains = []
    for index in domain_indices:
        url_filters = {}
        url_params = {}
        url_filters['domains'] = str(all_domains[index]).strip()

        #check if app supplies limit then append the parameter. Default set in conf file
        url_params['limit'] = str(config['DATA_PARAMS']['domain_limit_default']).strip()

        domain_words_list.append(build_and_execute_request(path,url_filters,url_params))
        #break
        
    return jsonify({'domains':domain_words_list})

if __name__ == '__main__':
    app.run(debug=True)